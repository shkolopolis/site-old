FROM alpine:3.8 as builder

COPY . /site
RUN apk add --no-cache git ca-certificates && \
    cd /site && \
    git clone -b v1.4.0 --depth 1 https://github.com/mtn/cocoa-eh-hugo-theme.git themes/cocoa-eh && \
    wget https://github.com/gohugoio/hugo/releases/download/v0.50/hugo_0.50_Linux-64bit.tar.gz -O hugo.tar.gz && \
    tar -xf hugo.tar.gz -C /usr/local/bin/ && \
    hugo

FROM nginx:alpine

COPY --from=builder /site/public /usr/share/nginx/html
