+++
title = "Home"
+++

![Startup](/images/logo.jpg)

# Салют, фратеры!

Здесь строится Школополис (в народе Шакалополис, ШколоPolice, Школололис... - список активно пополняется).

Выражаем особую благодарность анончикам, принимающим участие в строительстве:

- vito
- Невнучаев
- школота
- Vlad Chekunov
- коллектив букфагов

Сайт находится в стадии активной разработки!
